<?php

include('../autoload.php');

use Game\Fight;
use Game\SkillRapidStrike;


$fight = new Fight();

$fight->init();

$fight->displayPlayerStats();

while (!$fight->isGameOver()) {

    $fight->checkForSkill();

    $fight->strike();

    if ($fight->getSkill() instanceof SkillRapidStrike) {
        $fight->strike();
    }

    $fight->switchTurns();

    $fight->displayPlayerStats();
}

echo "<b>Winner is ". $fight->getWinner()."</b>";