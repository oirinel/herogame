<?php

namespace Game;

use Helper\Random;
use Player\Player;
use Player\Orderus;
use Player\WildBeast;

class Fight
{

    private Orderus $orderus;

    private WildBeast $wildBeast;

    private Player $attacker;

    private Player $defender;

    private $strike_count = 0;

    private $skill;


    public function setSkill($skill)
    {
        $this->skill = $skill;
    }

    public function getSkill()
    {
        return $this->skill;
    }

    public function init()
    {
        $this->orderus = new Orderus();
        $this->wildBeast = new WildBeast();

        $this->attacker = $this->whosFirst();
        $this->defender = ($this->attacker instanceof WildBeast) ? $this->orderus : $this->wildBeast;
    }


    private function whosFirst(): Player
    {
        if ($this->orderus->speed == $this->wildBeast->speed) {
            if ($this->orderus->luck > $this->wildBeast->luck) {
                return $this->orderus;
            } else {
                return $this->wildBeast;
            }
        }

        if ($this->orderus->speed > $this->wildBeast->speed) {
            return $this->orderus;
        }

        return $this->wildBeast;
    }


    // returns true if defender is lucky on that turn
    private function isDefenderLucky(): bool
    {
        return Random::tryMyLuck($this->defender->luck);
    }



    public function strike()
    {
        $this->strike_count++;

        if ($this->isDefenderLucky()) {
            echo sprintf('<b style="color: green">%d) %s was lucky, %s missed!</b><br />', $this->strike_count, $this->defender, $this->attacker);
            echo '<br />';
            return;
        }

        $damage = ($this->attacker->strength - $this->defender->defence);

        // damage should be positive or zero
        $damage = $damage > 0 ? $damage : 0;

        // magic shield active
        if ($this->getSkill() instanceof SkillMagicShield) {
            $damage = $damage / 2;
        }

        $this->defender->addDamage($damage);

        echo "<b>$this->strike_count) $this->attacker strikes $this->defender (damage: $damage)</b> <br />";
        echo '<br />';
    }


    // game over check
    public function isGameOver(): bool
    {
        return $this->strike_count >= 20
            || $this->orderus->health <= 0
            || $this->wildBeast->health <= 0
        ;
    }


    public function checkForSkill()
    {
        $this->setSkill(null);

        if ($this->attacker === $this->orderus) {
            $lucky = Random::tryMyLuck($this->orderus->getRapidStrikeChance());

            if ($lucky) {
                echo '<div style="color: red"><b>Rapid Strike enabled for Orderus!!!</b></div><br />';
                $this->setSkill(new SkillRapidStrike());
            }
        }

        if ($this->defender === $this->orderus) {
            $lucky = Random::tryMyLuck($this->orderus->getMagicShieldChance());

            if ($lucky) {
                echo '<div style="color: blue"><b>Magic Shield activated for Orderus!!!</b></div><br />';
                $this->setSkill(new SkillMagicShield());
            }
        }

    }

    public function switchTurns()
    {
        if ($this->attacker instanceof Orderus) {
            $this->attacker = $this->wildBeast;
            $this->defender = $this->orderus;
        } else {
            $this->attacker = $this->orderus;
            $this->defender = $this->wildBeast;
        }
    }


    public function displayPlayerStats()
    {
        echo "$this->attacker (attacker)<br />";
        $this->attacker->displayStats();

        echo '<br />';
        echo '<br />';

        echo "$this->defender (defender)<br />";
        $this->defender->displayStats();

        echo '<br />';
        echo '<br />';
    }


    function getWinner()
    {
        if ($this->wildBeast->health == $this->orderus->health) {
            return 'Draw';
        }

        return ($this->wildBeast->health > $this->orderus->health) ?
            $this->wildBeast : $this->orderus;

    }
}