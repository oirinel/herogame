<?php

namespace Player;

use Helper\Random;

class Player
{

    protected static $health_range;
    protected static $strength_range;
    protected static $defence_range;
    protected static $speed_range;
    protected static $luck_range;


    public $health;
    public $strength;
    public $defence;
    public $speed;
    public $luck;   // 0% means no luck, 100% lucky all the time



    function __construct()
    {
        $this->health = Random::getNumberBetween($this::$health_range);
        $this->strength = Random::getNumberBetween($this::$strength_range);
        $this->defence = Random::getNumberBetween($this::$defence_range);
        $this->speed = Random::getNumberBetween($this::$speed_range);
        $this->luck = Random::getNumberBetween($this::$luck_range);
    }

    function addDamage($damage)
    {
        $this->health -= $damage;

        if ($this->health < 0) {
            $this->health = 0;
        }
    }


    function displayStats()
    {
        echo "Health: $this->health | Strength: $this->strength | Defence: $this->defence | Speed: $this->speed | Luck: $this->luck";
    }
}