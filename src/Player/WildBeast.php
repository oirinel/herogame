<?php
namespace Player;

class WildBeast extends Player
{

    protected static $health_range = [60, 90];
    protected static $strength_range = [60, 90];
    protected static $defence_range = [40, 60];
    protected static $speed_range = [40, 60];
    protected static $luck_range = [25, 40];


    public function __toString()
    {
        return 'Wild Beast';
    }


}