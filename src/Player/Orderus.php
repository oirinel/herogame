<?php
namespace Player;

class Orderus extends Player
{

    protected static $health_range = [70, 100];
    protected static $strength_range = [70, 80];
    protected static $defence_range = [45, 55];
    protected static $speed_range = [40, 50];
    protected static $luck_range = [10, 30];

    // percentage for Rapid Strike to occur
    private $rapid_strike_chance = 10;

    // percentage for Magic Shield to occur
    private $magic_shield_chance = 20;


    public function __toString()
    {
        return 'Orderus';
    }


    public function getRapidStrikeChance()
    {
        return $this->rapid_strike_chance;
    }


    public function getMagicShieldChance()
    {
        return $this->magic_shield_chance;
    }
}