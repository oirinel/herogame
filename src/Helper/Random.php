<?php

namespace Helper;

class Random
{

    // random number between values provided as array elements
    public static function getNumberBetween(array $arr): int
    {
        $min = $arr[0];
        $max = $arr[1];

        mt_srand();

        return mt_rand($min, $max);
    }


    // chance to win with a probability
    // probability is provided as percent [0 - never, 100 - always]
    public static function tryMyLuck(int $probability): bool
    {
        $rand = mt_rand(1, 99);

        $win = $rand < $probability;

        return $win;
    }

}